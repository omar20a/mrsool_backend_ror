class InfosController < ApplicationController
	def new
		@info = Info.new
	end

	def info_params
    params.require(:info).permit(:Name, :Email, :Phone)
  	end


	def create
  	@info = Info.new(info_params)

  	if @info.save
  	
    redirect_to new_info_path
  	end

  	
	end
end
