class CreateInfos < ActiveRecord::Migration[5.1]
  def change
    create_table :infos do |t|
      t.string :Name
      t.string :Email
      t.string :Phone

      t.timestamps
    end
  end
end
